import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
    selector: 'sighUp-form',
    templateUrl: 'app/signup/signup.component.html',
    styleUrls: ['app/signup/signup.component.css'],
})

export class SignUPComponent 
{
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmPassword: string;
    dateOfBirth: Date;
    constructor(private router:Router){}
    /**
     * Add User in local Storage
    */
    public addUser() {
        var memoryObject = localStorage.getItem("users");
        if (memoryObject == null) 
        {
            var userObject: User = new User(this.firstName, this.lastName, this.email, this.password, this.dateOfBirth);
            var array = new Array();
            array.push(userObject);
            localStorage.setItem("users", JSON.stringify(array));
            alert("Data Saved Sucessfully");
            this.router.navigate(['/userprofile',this.firstName+" "+this.lastName]);        }
        else 
        {
            var array: any[] = JSON.parse(localStorage.getItem('users'));
            if (!this.isvalidUser(this.email)) 
            {
                alert("user aleardy Exists!!!!");
                return false;
            }
            var userObject: User = new User(this.firstName, this.lastName, this.email, this.password, this.dateOfBirth);
            array.push(userObject);
            localStorage.setItem("users", JSON.stringify(array));
            alert("Data Saved Sucessfully");
            this.router.navigate(['/userprofile',this.firstName+" "+this.lastName]);
            return false;
        }
    }
    /**
     * Validate Usre email Id for dublication Check 
     * @param email 
     */
    public isvalidUser(email: string) 
    {
        if (localStorage.getItem("users") != null) 
        {
           var array: any[] = JSON.parse(localStorage.getItem('users'));
            for (var i = 0; i < array.length; i++) {
                if (array[i].email == email) 
                {
                    return false;
                }
            }
        }
        return true;
    }
}

class User 
{
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmPassword:string;
    dateOfBirth: Date;
    constructor(firstName: string, lastName: string,  email: string,password: string, dateOfBirth: Date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
    }
}