"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var SignUPComponent = (function () {
    function SignUPComponent(router) {
        this.router = router;
    }
    SignUPComponent.prototype.addUser = function () {
        var memoryObject = localStorage.getItem("users");
        if (memoryObject == null) {
            var userObject = new User(this.firstName, this.lastName, this.email, this.password, this.dateOfBirth);
            var array = new Array();
            array.push(userObject);
            localStorage.setItem("users", JSON.stringify(array));
            alert("Data Saved Sucessfully");
            this.router.navigate(['/userprofile', this.firstName + " " + this.lastName]);
        }
        else {
            var array = JSON.parse(localStorage.getItem('users'));
            if (!this.isvalidUser(this.email)) {
                alert("user aleardy Exists!!!!");
                return false;
            }
            var userObject = new User(this.firstName, this.lastName, this.email, this.password, this.dateOfBirth);
            array.push(userObject);
            localStorage.setItem("users", JSON.stringify(array));
            alert("Data Saved Sucessfully");
            this.router.navigate(['/userprofile', this.firstName + " " + this.lastName]);
            return false;
        }
    };
    SignUPComponent.prototype.isvalidUser = function (email) {
        if (localStorage.getItem("users") != null) {
            var array = JSON.parse(localStorage.getItem('users'));
            for (var i = 0; i < array.length; i++) {
                if (array[i].email == email) {
                    return false;
                }
            }
        }
        return true;
    };
    return SignUPComponent;
}());
SignUPComponent = __decorate([
    core_1.Component({
        selector: 'sighUp-form',
        templateUrl: 'app/signup/signup.component.html',
        styleUrls: ['app/signup/signup.component.css'],
    }),
    __metadata("design:paramtypes", [router_1.Router])
], SignUPComponent);
exports.SignUPComponent = SignUPComponent;
var User = (function () {
    function User(firstName, lastName, email, password, dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
    }
    return User;
}());
//# sourceMappingURL=signup.component.js.map