"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var login_component_1 = require("./login/login.component");
var userprofile_component_1 = require("../app/userprofile/userprofile.component");
var signup_component_1 = require("./signup/signup.component");
var routes = [
    { path: '', redirectTo: '/index', pathMatch: 'full' },
    { path: 'index', component: login_component_1.Login },
    { path: 'login', component: login_component_1.Login },
    { path: 'signUp', component: signup_component_1.SignUPComponent },
    { path: "userprofile", component: userprofile_component_1.UserProfile },
    { path: "userprofile/:name", component: userprofile_component_1.UserProfile },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [
            router_1.RouterModule.forRoot(routes)
        ],
        exports: [
            router_1.RouterModule
        ]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
exports.routingComponent = [userprofile_component_1.UserProfile, login_component_1.Login, signup_component_1.SignUPComponent];
//# sourceMappingURL=app.routing.module.js.map