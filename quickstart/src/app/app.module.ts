import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { AppRoutingModule,routingComponent } from "./app.routing.module";
import { AppComponent }  from './app.component';
import { LoginComponent } from "./login.component";
import { Login } from "./login/login.component";
import { SignUPComponent } from "./signup/signup.component";
import { UserProfile } from "./userprofile/userprofile.component";
@NgModule({
  imports:      [ BrowserModule,FormsModule,AppRoutingModule],
  declarations: [ AppComponent,LoginComponent,Login,SignUPComponent,routingComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
