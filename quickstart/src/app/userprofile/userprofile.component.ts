import { Component,OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
    templateUrl:'/app/UserProfile/userprofile.component.html',
    styleUrls:['/app/userprofile/userprofile.component.css'],
})
export class UserProfile implements OnInit
{
    public name:string;
    public users: any[];
    ngOnInit(): void 
    {
        let name=this.route.snapshot.params['name'];
        this.name=name;
    }

    constructor(private route:ActivatedRoute)
    {
        this.getData();        
    }
    /**
     * Set the Data in Usres Array for Rendering 
     */
    getData()
    {
        if(localStorage.getItem("users")!=null)
        {
            this.users=JSON.parse(localStorage.getItem("users"));
        }
    }
}