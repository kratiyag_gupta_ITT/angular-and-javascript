import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";

import { Login } from "./login/login.component";
import { UserProfile } from "../app/userprofile/userprofile.component";
import { SignUPComponent } from "./signup/signup.component";


const routes:Routes=[
    {path:'',redirectTo:'/index',pathMatch:'full'},
    {path:'index',component:Login},
    {path:'login',component:Login},
    {path:'signUp',component:SignUPComponent},
    {path:"userprofile",component:UserProfile},
    {path:"userprofile/:name",component:UserProfile},
];

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]
})

export class AppRoutingModule{}
export const routingComponent=[UserProfile,Login,SignUPComponent]