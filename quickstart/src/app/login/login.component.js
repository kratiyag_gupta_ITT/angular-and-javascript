"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Login = (function () {
    function Login(router) {
        this.router = router;
    }
    Login.prototype.findUser = function () {
        var flag = 0;
        if (this.isValidInput(this.userName, this.password)) {
            var memoryObject = localStorage.getItem('users');
            var array = null;
            if (memoryObject == null) {
                alert("no user Exist");
            }
            else {
                array = JSON.parse(memoryObject);
                for (var i = 0; i < array.length; i++) {
                    if (array[i].email === this.userName && array[i].password === this.password) {
                        flag = 1;
                        this.router.navigate(['/userprofile', array[i].firstName + " " + array[i].lastName]);
                    }
                }
                if (flag == 0) {
                    alert("User Name or Password is Incorrect");
                    flag = 0;
                }
            }
            return false;
        }
    };
    Login.prototype.isValidInput = function (userName, password) {
        if (userName == null || /\s/.test(userName) || userName == '') {
            alert("username cannot be Empty");
            return false;
        }
        if (password == null || /\s/.test(password) || password == '') {
            alert("Password Cannot be Empty");
            return false;
        }
        return true;
    };
    return Login;
}());
Login = __decorate([
    core_1.Component({
        selector: 'login-form',
        templateUrl: 'app/login/login.component.html',
        styleUrls: ['app/login/login.component.css'],
    }),
    __metadata("design:paramtypes", [router_1.Router])
], Login);
exports.Login = Login;
//# sourceMappingURL=login.component.js.map