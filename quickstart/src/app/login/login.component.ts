import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector:'login-form',
    templateUrl:'app/login/login.component.html',
    styleUrls:['app/login/login.component.css'],
})

export class Login
{
    public userName:string;
    public password:string;
    constructor(private router:Router){}
    /**
     * Find User In Local Storage with specified UserName and Password 
     */
    public findUser()
    {
        var flag=0;
        if(this.isValidInput(this.userName,this.password))
        {
            var memoryObject=localStorage.getItem('users');
            var array=null;
            if(memoryObject==null)
            {
                alert("no user Exist")
            }
            else
            {
                array=JSON.parse(memoryObject);
                for(var i=0;i<array.length;i++)
                {
                    if(array[i].email===this.userName&&array[i].password===this.password)
                    {
                        flag=1;
                        this.router.navigate(['/userprofile',array[i].firstName+" "+array[i].lastName]);
                    }
                }
                if(flag==0)
                {
                    alert("User Name or Password is Incorrect");
                    flag=0;
                }
            }
            return false;
        }
    }
    /**
     * For Validating UsreName and Password
     * @param userName 
     * @param password 
     */
    isValidInput(userName:string,password:string) 
    {
        if(userName==null||/\s/.test(userName)||userName=='')
        {
            alert("username cannot be Empty")
            return false;
        }
        if(password==null||/\s/.test(password)||password=='')
        {
            alert("Password Cannot be Empty")
            return false;
        }
        return true;
        
    }
}
