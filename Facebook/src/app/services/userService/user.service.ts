import { Injectable } from "@angular/core";
import { Http, Response, Headers, URLSearchParams, RequestOptions } from "@angular/http";
//import { Observable } from "rxjs";

import { User } from "../../models/user.model";

@Injectable()
export class UserService {
    constructor(private http: Http) { }

    /**
     * create users in Database
     * @param user 
     */
    createUser(user: User) {
        alert("inside create user");
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post("http://localhost:8080/Facebook/getUserData", user, options);
    }
    
    /**
     * Get user data from database
     * @param res 
     */
    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    /**
     * Throws and print erron on console
     * @param error 
     */
    private handleError(error: Response | any) {
        console.error(error.message || error);
    }
}