import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";

import { AppRoutingModule,routingComponent } from "./app.routing.module";
import { AppComponent }  from './app.component';
import { Login } from "./login/login.componet";
import { LoginSignup } from "./loginSignup/loginSignup.component";
import { Signup } from "./signup/signup.component";
import { FbHeader } from "./components/fb-header/fbheader.component";
import { UserPage } from "./components/userpage/userpage.component";
import { UsersPost } from "./components/postcomponent/post.component";
import { Feeling } from "./components/feelingbox/feelingbox.component";
import { UserService } from "./services/userService/user.service";
import { CommentComponent } from "./components/comment/comment.component";

@NgModule({
  imports:      [ BrowserModule,FormsModule,HttpModule,AppRoutingModule],
  declarations: [ AppComponent,Login,LoginSignup,Signup,FbHeader,UserPage,UsersPost,Feeling,routingComponent,CommentComponent],
  providers:[UserService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
