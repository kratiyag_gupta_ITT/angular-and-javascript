import { Component,OnInit } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/toPromise";

@Component({
  selector: 'my-app',
  template: `
  <router-outlet></router-outlet>
  `,
})
export class AppComponent implements OnInit 
{ 
  public users:any[];
  constructor(private http:Http){}
  ngOnInit(): void 
  {
    this.http.get("http://localhost:8080/Facebook/getUserData")
    .toPromise().then(r=> r.json()).then(r=>this.users=r);
  
  }
  name = 'Angular';
 }
