/**
 * Copy of POJO class to hold user Data
 */
export class Comment
{
    commentId:number;
    postId:number;
    userId:number;
    commentData:string;
    userName:string;
    time:string;
    constructor(postId:number,commentData:string,userName:string,time:string)
    {
        this.commentId=null;
        this.postId=postId;
        this.userId=null;
        this.commentData=commentData;
        this.userName=userName;
        this.time=time;
    }
}