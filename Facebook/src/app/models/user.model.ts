export class User
{
    public userId:number;
    public firstName:string;
    public lastName:string;
    public email:string;
    public mobileNumber:string;
    public password:string;
    public dateOfBirth:string;
    public gender:string;
    constructor(firstName:string,lastName:string,email:string,mobileNumber:string,password:string,dateOfBirth:string,gender:string)
    {
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.mobileNumber=mobileNumber;
        this.password=password;
        this.dateOfBirth=dateOfBirth;
        this.gender=gender;
    }

}