import { Component, OnInit } from "@angular/core";
import { UserService } from "../services/userService/user.service";
import { User } from "../models/user.model";
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
@Component({
    selector: 'signup-form',
    templateUrl: 'app/signup/signup.component.html',
    styleUrls: ['app/signup/signup.component.css'],
})

export class Signup implements OnInit 
{
    public userId:number;
    public firstName:string;
    public lastName:string;
    public email:string;
    public mobileNumber:string;
    public password:string;
    public dateOfBirth:string;
    public gender:string;
    statusCode: number;
    requestProcessing = false;
    //userIdToUpdate = null;
    constructor(private userService: UserService,private http:Http,private router:Router) { }
    ngOnInit(): void {
    
    }
    addUser()
    {
        var user:User=new User(this.firstName,this.lastName,this.email
            ,this.mobileNumber,this.password,this.dateOfBirth,this.gender);
        //alert("inside add user"+JSON.stringify(user));
        var responce:any=this.http.post("http://localhost:8080/Facebook/createUser",user)
        .map(success=>success.status).catch(this.handleError).subscribe(successCode=>{
            this.statusCode=successCode;
        },
        errorCode => {this.statusCode = errorCode});
        //alert(this.handleError);
         // alert("data submitted sucessfully");
        sessionStorage.setItem("email",this.firstName+" "+this.lastName); 
        return false;
    }
    preProcessConfigurations() {
        this.statusCode = null;
        this.requestProcessing = true;
    }
    private handleError (error: Response | any) {
        //console.error(error.message || error);
        return Observable.throw(error.status);
        }
        navigateToUserPage()
        {
            sessionStorage.setItem("emailId",this.email);
            this.router.navigate(['/accountCreated']);
        }

}