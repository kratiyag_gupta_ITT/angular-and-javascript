import { Component, OnInit, Input } from "@angular/core";
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import { FeelingBox } from "../../models/fellingBox.model";
import { UsersPost } from "../postcomponent/post.component";
@Component({
    selector: 'feeling-box',
    templateUrl: 'app/components/feelingbox/feelingbox.component.html',
    styleUrls: ['app/components/feelingbox/feelingbox.component.css'],
})
/**
 * Feeling Box for posting the comments
 */
export class Feeling {
    @Input()
    emailId: string;
    username = sessionStorage.getItem("emailId");
    userId: number;
    postData: string;
    image: string;
    heading: string;
    postDate: Date;
    postTime: string;
    email: number;
    statusCode: number;
    public users: any[];
    constructor(private http: Http, private router: Router) { }
    /**
    * Get Post Data from database
    */
    getfeelingpostData() {
        let d: Date = new Date();
        let date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
        let time = d.getHours + ":" + d.getMinutes;
        this.image = (<HTMLImageElement>document.getElementById("uploaded-image"))
            .src;
        var fb: FeelingBox = new FeelingBox(this.postData, date, time, this.image);
        this.http.post("http://localhost:8080/Facebook/savecomment/" + this.emailId, fb).map(success => success.status).catch(this.handleError).subscribe(successCode => {
            this.statusCode = successCode;
        },
            errorCode => this.statusCode = errorCode);
            location.reload();
            
    }
    /**
     * Print Error Message to console and throw Error
     * @param error 
     */
    private handleError(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
    }

    /**
     * Invokes when image is uploaded
     * @param event 
     */
    handleInputChange(event: any) {
        var pattern = /image-*/;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            this.image = URL.createObjectURL(event.target.files[0]);
            reader.onloadend = function () {
                (<HTMLImageElement>document.getElementById("uploaded-image"))
                    .src = reader.result;
                console.log('RESULT', reader.result)
            }
            reader.readAsDataURL(event.target.files[0]);

        }

    }
}