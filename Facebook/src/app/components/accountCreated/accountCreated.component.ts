import { Component } from "@angular/core";
import { Http } from "@angular/http";
import { Router } from "@angular/router";
@Component({
    selector:'account-created-msg',
    templateUrl:'app/components/accountCreated/accountCreated.component.html',
    styleUrls:['app/components/accountCreated/accountCreated.component.css'],
})
export class AccountHandler
{
    userName:string;
    constructor(private router:Router)
    {
        this.userName=sessionStorage.getItem("emailId");
    }
    redirect()
    {
        this.router.navigate(['/usersPage',this.userName]);
    }
}