import { Component } from "@angular/core";

@Component({
        selector:'fb-header',
        templateUrl:'app/components/fb-header/fbheader.component.html',
        styleUrls:['app/components/fb-header/fbheader.component.css'],
})

export class FbHeader
{
        //save user emailId in local storage for faster retrival
        username=sessionStorage.getItem("emailId");
}