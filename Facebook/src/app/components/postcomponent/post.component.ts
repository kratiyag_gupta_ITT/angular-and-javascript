import { Component, Input } from "@angular/core";
import { Http } from "@angular/http";
import { Comment } from "../../models/comment.model";
import { Like } from "../../models/like.model";

@Component({
    selector: 'users-post',
    templateUrl: 'app/components/postcomponent/post.component.html',
    styleUrls: ['app/components/postcomponent/post.component.css'],
})

export class UsersPost {
    emailId: string = sessionStorage.getItem("emailId");
    public posts: any[];
    public likeusres: any[];
    constructor(private http: Http) { }

    /**
     * Get comments through http API
     */
    ngOnInit(): void {
        this.emailId = sessionStorage.getItem("emailId");
        this.http.get("http://localhost:8080/Facebook/getPost/" + this.emailId)
            .toPromise().then(r => r.json()).then(r => this.posts = r);
    }

    /**
     * Store Likes 
     * @param event Dom binding for fetching postId 
     */

    storeLike(event: any) {
        var target = event.target || event.srcElement || event.currentTarget;
        var postId = event.currentTarget.id;
        let like: Like = new Like();
        like.postId = postId;
        like.userId = this.emailId;
        this.http.post("http://localhost:8080/Facebook/increaseLike", like).map(success => success.status).subscribe(
            successCode => {
                document.getElementById("like-viewer" + postId).innerHTML = parseInt(document.getElementById("like-viewer" + postId).innerHTML) + 1 + "";
                document.getElementById(postId).innerHTML = "UnLike"
            }
        );
    }

    /**
     * Get number of users who liked the post
     * @param postId 
     */

    getLikedUsers(postId: any) {
        document.getElementById("liked-users" + postId).style.display = "block";
        this.http.get("http://localhost:8080/Facebook/likeUserName/" + postId).
            toPromise().then(r => r.json()).then(r => this.likeusres = r);
    }

    /**
     * Hide popup
     * @param postId 
     */

    hideLikePopup(postId: any) {
        document.getElementById("liked-users" + postId).style.display = "none";
        this.likeusres = null;
    }
}