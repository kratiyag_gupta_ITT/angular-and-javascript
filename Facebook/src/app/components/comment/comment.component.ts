import { Component,Input } from "@angular/core";
import { Http } from "@angular/http";
import { Comment } from "../../models/comment.model";
@Component({
    selector:'comment-component',
    templateUrl:'app/components/comment/comment.component.html',
    styleUrls:['app/components/comment/comment.component.css'],   
})

/**
 * Comment Class Handles the Events for Comments
 */
export class CommentComponent
{ 
    @Input() postId: number;
    comments:any[];
    constructor(private http:Http){}
    emailId:string=sessionStorage.getItem("emailId");
    
    /**
     * Store Comments to database
     * @param event     Bind dom event
     */
    storeComment(event:any):void
    {
        var target = event.target || event.srcElement || event.currentTarget;
        var idAttr = target.attributes.id;
        var postId = idAttr.nodeValue;
        var d:Date=new Date();
        var time=d.getHours()+":"+d.getMinutes();
        var commentData:string=(document.getElementById("comment-box"+postId) as HTMLInputElement).value;
        var comment:Comment=new Comment(postId,commentData,this.emailId,time);
        this.http.post("http://localhost:8080/Facebook/comment/"+this.emailId,comment).subscribe(); 
        this.http.get("http://localhost:8080/Facebook/getcomment/"+postId)
        .toPromise().then(r=> r.json()).then(r=>this.comments=r);
 
    }
    
    /**
     * Get Comments from Database 
     * @param event 
     */
    getCommentData(event:any)
    {
        var target = event.target || event.srcElement || event.currentTarget;
        var idAttr = target.attributes.id;
        var postId = idAttr.nodeValue;  //post Id for current post
        this.http.get("http://localhost:8080/Facebook/getcomment/"+postId)
        .toPromise().then(r=> r.json()).then(r=>this.comments=r);
    }
}