import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
@Component({
    selector:'user-page',
    templateUrl:'app/components/userpage/userpage.component.html',
    styleUrls:['app/components/userpage/userpage.component.css'],
})

export class UserPage
{
    public userId:string;
    ngOnInit(): void 
    {
        let userId=this.route.snapshot.params['userId'];
        this.userId=userId;
    }
    constructor(private route:ActivatedRoute)
    { }
    
}