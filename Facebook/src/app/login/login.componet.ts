import { Component } from "@angular/core";
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";


import { LoginModel } from "../models/login.model";
@Component({
    selector: 'login-form',
    templateUrl: 'app/login/login.component.html',
    styleUrls: ['app/login/login.component.css'],
})

export class Login {
    public userName: string;
    public password: string;
    statusCode: number;
    constructor(private http: Http, private router: Router) { }
    /**
     * Validate UserName and Password of User
     */
    validateUser() {
        var login = new LoginModel(this.userName, this.password);
        this.http.post("http://localhost:8080/Facebook/validateUser", login)
            .map(success => success.status).catch(this.handleError).subscribe(successCode => {
                this.statusCode = successCode;
            },
            errorCode => this.statusCode = errorCode);
    }
    
    /**
     * Return Error and print respponce on Console
     * @param error 
     */
    private handleError(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
    }

    navigateToUserPage() {
        sessionStorage.setItem("emailId", this.userName);1
        this.router.navigate(['/usersPage', this.userName]);
    }
}