import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";

import { Login } from "./login/login.componet";
import { LoginSignup } from "./loginSignup/loginSignup.component";
import { Signup } from "../app/signup/signup.component";
import { UserPage } from "../app/components/userpage/userpage.component";
import { AccountHandler } from "../app/components/accountCreated/accountCreated.component";

const routes:Routes=[
    {path:'',redirectTo:'/index',pathMatch:'full'},
    {path:'index',component:LoginSignup},
    {path:'usersPage/:userId',component:UserPage},
    {path:'accountCreated',component:AccountHandler},
];

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]
})

export class AppRoutingModule{}
export const routingComponent=[Login,LoginSignup,Signup,UserPage,AccountHandler]