class PersonInformation
{
     firstName:string;
     lastName:string;
     emailId:string;
     password:string;
     confirmPassword:string;
     constructor(firstName:string,lastName:string,emailId:string,password:string,confirmPassword:string)
     {
         this.firstName=firstName;
         this.lastName=lastName;
         this.emailId=emailId;
         this.password=password;
         this.confirmPassword=confirmPassword;
     }
}

class Operations
{

     getData():PersonInformation
    {
        //var array:any[]=new Array();
        var firstName=(<HTMLInputElement>document.getElementById("first_name")).value;
        var lastName=(<HTMLInputElement>document.getElementById("signup_lastname")).value;
        var emailId=(<HTMLInputElement>document.getElementById("signup_username")).value;
        var password=(<HTMLInputElement>document.getElementById("signup_password")).value;
        var confirmPassword=(<HTMLInputElement>document.getElementById("confirmation_password")).value;
        var person:PersonInformation=new PersonInformation(firstName,lastName,emailId,password,confirmPassword);
        return person;
    }
    storeInformation():boolean
    {
        var array:any[]=new Array();
        var local=localStorage.getItem("users");
        var currentUser=this.getData();
        if(local==null)
            {
              if(!this.validate(array,currentUser))
                {
                    return false;
                }
                array.push(this.getData());
                localStorage.setItem("users",JSON.stringify(array));
            }
        else
            {
                array=JSON.parse(localStorage.getItem("users"));
                if(!this.validate(array,this.getData()))
                {
                    return false;
                }
                array.push(this.getData());
                localStorage.setItem("users",JSON.stringify(array));
                window.location.href="profile.html";
                return false;
            }
            return false;
    }
    loginUser():boolean
    {
        var email:string=(<HTMLInputElement>document.getElementById("login_username")).value;
        var password:string=(<HTMLInputElement>document.getElementById("login_password")).value;
        var array:any[]=new Array();
        var local=localStorage.getItem("users");
        if(local==null)
            {
                alert("Invalid User Name and PAssword");
                return false;
            }
        else
            {
                array=JSON.parse(localStorage.getItem("users"));
                for(var i=0;i<array.length;i++)
                    {
                        if(array[i].emailId==email&&array[i].password==password)
                            {
                                window.location.href="profile.html";
                                return false;
                            }
                    }
                alert("Invalid User Name and Password");
            }
            return false;
    }
    validate(array,currentUser):boolean
    {
        if(array!==null)
            {
                for(var i=0;i<array.length;i++)
                {
                    if(array[i].emailId==currentUser.emailId)
                    {
                       alert("Email id already Exist");
                       return false;
                    }
                }
            }
            /**
             * Password Validation
             */
        if(currentUser.password!== currentUser.confirmPassword)
            {
                alert("Confirm Password and Password is not equal");
                return false;
            }
            /**
             * Validation for Spaces in First name and Last Name
             */
        if(currentUser.firstName.trim(" ")==0||currentUser.lastName.trim(" ")==0)
            {
                alert("First Name and Last Name fields can not be blank");
                return false;
            }
            /**
             * Validation for numbers and Special Symbols
             */
        if(this.hasNumbers(currentUser.firstName)||this.hasNumbers(currentUser.lastName)
            ||this.hasSpecialSymbol(currentUser.firstName)||this.hasSpecialSymbol(currentUser.lastName))
           {
               alert("First Name and Last Name Cannot contain Numbers and special Symbol");
                return false;
           }    
        return true;
    }
    /**
     * Function check for numbers in string
     * @param name 
     */
    hasNumbers(name):boolean
    {
        return /\d/.test(name);
    }
    /**
     * Function Check for special Character
     * @param name 
     */
    hasSpecialSymbol(name):boolean
    {
       return /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(name); 
    }
}

