
./* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this te00000000mplate file, choose Tools | Templates
 * and open the template in the editor.
 */
var user;
function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function getInformation()
{
    var foo = getParameterByName('uname');
    user = JSON.parse(sessionStorage.getItem(foo));
    var u = JSON.parse(localStorage.getItem(user.userName));
    //document.getElementById('d').src =u.signUrl;
}

function init() {
    var el = document.getElementById('c');
    var ctx = el.getContext('2d');
    var isDrawing;

    el.onmousedown = function (e) {
        isDrawing = true;
        ctx.moveTo(e.clientX, e.clientY);
    };
    el.onmousemove = function (e) {
        if (isDrawing) {
            ctx.lineTo(e.clientX, e.clientY);
            ctx.stroke();
        }
    };
    el.onmouseup = function () {
        isDrawing = false;
    };
}

function cleanCanvas()
{
    //var el = document.getElementById('c');
    //var ctx = el.getContext('2d');
    location.reload();
}

function addSignature()
{
    var canvas = document.getElementById('c');
    var context = canvas.getContext('2d');
    var sign = canvas.toDataURL();
    u = JSON.parse(localStorage.getItem(user.userName));
    u.signUrl = sign;
    localStorage.setItem(u.userName, JSON.stringify(u));
    document.getElementById('d').src = u.signUrl;
    alert("Signature Added Sucessfully");

}