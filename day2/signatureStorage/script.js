
// Set-up the canvas and add our event handlers after the page has loaded
function init() 
{
  var el = document.getElementById('c');
  var ctx = el.getContext('2d');
  var isDrawing;

  el.onmousedown = function (e)
  {
    isDrawing = true;
    ctx.moveTo(e.clientX, e.clientY);
  };
  el.onmousemove = function (e) 
    {
    if (isDrawing) {
      ctx.lineTo(e.clientX, e.clientY);
      ctx.stroke();
    }
  };
  el.onmouseup = function () 
  {
    isDrawing = false;
  };
}
function getParameterByName(name, url) 
{
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function addSignature() 
{

  alert("hello");
  var foo = getParameterByName('uname');
  var user = JSON.parse(sessionStorage.getItem(foo));
  var canvas = document.getElementById('c');
  var context = canvas.getContext('2d');
  var dataURL = canvas.toDataURL();
  alert("hello");
  document.getElementById('d').src = dataURL;
  alert("hello");
}
