/**
 * Main Class Holdes all the Methods for game Implementation
 */
class TileGame
{
    static rightPressed: boolean = false;
    static leftPressed: boolean = false;
    static ballRadius: number = 10;
    static x: number = 480 / 2;
    static y: number = 320 - 20;
    static dx: number = 2;
    static dy: number = -2;
    static paddleHeight: number = 10;
    static paddleWidth: number = 75;
    static canvasHeight: number = 320;
    static canvasWidth: number = 480;
    static paddleX: number = (480 - TileGame.paddleWidth) / 2;
    static brickRowCount: number = 5;
    static brickColumnCount: number = 3;
    static brickWidth: number = 75;
    static brickHeight: number = 20;
    static brickPadding: number = 10;
    static brickOffsetTop: number = 30;
    static brickOffsetLeft: number = 30;
    static score: number = 0;
    static bricks:any = [];
    /**
     * For Drawing all the components of game on Main Canvas and Handling Events
     */
    draw()
    {
        var can = <HTMLCanvasElement>document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        ctx.clearRect(0, 0, can.width, can.height);
        var tileGame:TileGame=new TileGame();
        var ball:Ball=new Ball();
        var paddle:Paddle=new Paddle();
        var brick:Brick=new Brick();
        var eventHandler:EventHandler=new EventHandler();
        brick.drawBrick();
        eventHandler.addEvent();
        if (TileGame.x + TileGame.dx > can.width - TileGame.ballRadius || TileGame.x + TileGame.dx < TileGame.ballRadius) 
        {
            TileGame.dx = -TileGame.dx;
        }
        if (TileGame.y + TileGame.dy < TileGame.ballRadius) 
        {
            TileGame.dy = -TileGame.dy;
        }
        else if (TileGame.y + TileGame.dy > can.height - TileGame.ballRadius) 
        {
            if (TileGame.x > TileGame.paddleX-10 && TileGame.x < TileGame.paddleX+10 + TileGame.paddleWidth) 
            {
                TileGame.dy = -TileGame.dy;
            }
            else 
            {
                alert("GAME OVER!!!!! \nTry Again");
                document.location.reload();
            }
        }
        if (TileGame.rightPressed && TileGame.paddleX < can.width - TileGame.paddleWidth) 
        {
            TileGame.paddleX += 3;
        }
        else if (TileGame.leftPressed && TileGame.paddleX > 0) 
        {
            TileGame.paddleX -= 3;
        }
        tileGame.collisionDetectionBricks();
        ball.drawBall();
        paddle.drawPaddel();
        TileGame.x += TileGame.dx;
        TileGame.y += TileGame.dy;
    }
    /**
     * Collision Detection Logic between bricks and tiles
     */
    collisionDetectionBricks()
    {
        for (var c = 0; c < TileGame.brickColumnCount; c++) 
        {
            for (var r = 0; r < TileGame.brickRowCount; r++) 
            {
                var b = TileGame.bricks[c][r];
                if (b.status == 1) {
                    if (TileGame.x > b.x && TileGame.x < b.x + TileGame.brickWidth && TileGame.y > b.y && TileGame.y < b.y + TileGame.brickHeight) 
                    {
                        TileGame.dy = -TileGame.dy;
                        b.status = 0;
                        TileGame.score++;
                        if (TileGame.score == TileGame.brickRowCount * TileGame.brickColumnCount) 
                        {
                            alert("YOU WIN!!!!!");
                            document.location.reload();
                        }
                    }
                }
            }
        }
    }
    /**
     * Call draw function on page load and Initialize brick positions in Bricks array
     */
   callback() 
    {
        var canvas = <HTMLCanvasElement>document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        for (var c = 0; c < TileGame.brickColumnCount; c++) {
            TileGame.bricks[c] = [];
            for (var r = 0; r < TileGame.brickRowCount; r++) {
                TileGame.bricks[c][r] = { x: 0, y: 0, status: 1 };
            }
        }
        new TileGame().draw();
    }
}

class Ball
{
    /**
     * Draw Ball on the Canvas
     */
    drawBall()
    {
        var can = <HTMLCanvasElement>document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        ctx.beginPath();
        ctx.arc(TileGame.x, TileGame.y, TileGame.ballRadius, 0, Math.PI * 2);
        ctx.fillStyle = "gray";
        ctx.fill();
        ctx.closePath();
    }
    
}

class Brick
{
    /**
     * Draw Bricks on Canvas
     */
    drawBrick()
    {
        var can = <HTMLCanvasElement>document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        for (var c = 0; c < TileGame.brickColumnCount; c++) 
            {
             for (var r = 0; r < TileGame.brickRowCount; r++) 
                {
                 if (TileGame.bricks[c][r].status == 1)
                 {
                    var brickX = (r * (TileGame.brickWidth + TileGame.brickPadding)) + TileGame.brickOffsetLeft;
                    var brickY = (c * (TileGame.brickHeight + TileGame.brickPadding)) + TileGame.brickOffsetTop;
                    TileGame.bricks[c][r].x = brickX;
                    TileGame.bricks[c][r].y = brickY;
                    ctx.beginPath();
                    ctx.rect(brickX, brickY, TileGame.brickWidth, TileGame.brickHeight);
                    ctx.fillStyle = "gray";
                    ctx.fill();
                    ctx.closePath();
                }
            }
        }
    }
}

class Paddle
{
    /**
     * Draw Paddel on the Canvas
     */
    drawPaddel()
    {
        var can = <HTMLCanvasElement>document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        ctx.rect(TileGame.paddleX, TileGame.canvasHeight - TileGame.paddleHeight, TileGame.paddleWidth, TileGame.paddleHeight);
        ctx.fillStyle = "gray";
        ctx.fill();
        ctx.closePath();
    }
}

/**
 * Class for Handeling Key Events
 */
class EventHandler
{
    addEvent()
    {
        document.addEventListener("keydown", keyDownHandler, false);
        document.addEventListener("keyup", keyUpHandler, false);

        function keyDownHandler(e) 
        {
            if (e.keyCode == 39) 
            {
                TileGame.rightPressed = true;
            }
            else if (e.keyCode == 37) 
            {
                TileGame.leftPressed = true;
            }
            else if(e.keyCode == 32)
            {
                var tileGame:TileGame=new TileGame();
                //tileGame.callback();
                setInterval(tileGame.draw, 10);
            }
        }
        function keyUpHandler(e) 
        {
            if (e.keyCode == 39) 
            {
                TileGame.rightPressed = false;
            }
            else if (e.keyCode == 37) 
            {
                TileGame.leftPressed = false;
            }
        }
        
    }
}