/**
 * Main Class Holdes all the Methods for game Implementation
 */
var TileGame = /** @class */ (function () {
    function TileGame() {
    }
    /**
     * For Drawing all the components of game on Main Canvas and Handling Events
     */
    TileGame.prototype.draw = function () {
        var can = document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        ctx.clearRect(0, 0, can.width, can.height);
        var tileGame = new TileGame();
        var ball = new Ball();
        var paddle = new Paddle();
        var brick = new Brick();
        var eventHandler = new EventHandler();
        brick.drawBrick();
        eventHandler.addEvent();
        if (TileGame.x + TileGame.dx > can.width - TileGame.ballRadius || TileGame.x + TileGame.dx < TileGame.ballRadius) {
            TileGame.dx = -TileGame.dx;
        }
        if (TileGame.y + TileGame.dy < TileGame.ballRadius) {
            TileGame.dy = -TileGame.dy;
        }
        else if (TileGame.y + TileGame.dy > can.height - TileGame.ballRadius) {
            if (TileGame.x > TileGame.paddleX - 10 && TileGame.x < TileGame.paddleX + 10 + TileGame.paddleWidth) {
                TileGame.dy = -TileGame.dy;
            }
            else {
                alert("GAME OVER!!!!! \nTry Again");
                document.location.reload();
            }
        }
        if (TileGame.rightPressed && TileGame.paddleX < can.width - TileGame.paddleWidth) {
            TileGame.paddleX += 3;
        }
        else if (TileGame.leftPressed && TileGame.paddleX > 0) {
            TileGame.paddleX -= 3;
        }
        tileGame.collisionDetectionBricks();
        ball.drawBall();
        paddle.drawPaddel();
        TileGame.x += TileGame.dx;
        TileGame.y += TileGame.dy;
    };
    /**
     * Collision Detection Logic between bricks and tiles
     */
    TileGame.prototype.collisionDetectionBricks = function () {
        for (var c = 0; c < TileGame.brickColumnCount; c++) {
            for (var r = 0; r < TileGame.brickRowCount; r++) {
                var b = TileGame.bricks[c][r];
                if (b.status == 1) {
                    if (TileGame.x > b.x && TileGame.x < b.x + TileGame.brickWidth && TileGame.y > b.y && TileGame.y < b.y + TileGame.brickHeight) {
                        TileGame.dy = -TileGame.dy;
                        b.status = 0;
                        TileGame.score++;
                        if (TileGame.score == TileGame.brickRowCount * TileGame.brickColumnCount) {
                            alert("YOU WIN!!!!!");
                            document.location.reload();
                        }
                    }
                }
            }
        }
    };
    /**
     * Call draw function on page load and Initialize brick positions in Bricks array
     */
    TileGame.prototype.callback = function () {
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        for (var c = 0; c < TileGame.brickColumnCount; c++) {
            TileGame.bricks[c] = [];
            for (var r = 0; r < TileGame.brickRowCount; r++) {
                TileGame.bricks[c][r] = { x: 0, y: 0, status: 1 };
            }
        }
        new TileGame().draw();
    };
    TileGame.rightPressed = false;
    TileGame.leftPressed = false;
    TileGame.ballRadius = 10;
    TileGame.x = 480 / 2;
    TileGame.y = 320 - 20;
    TileGame.dx = 2;
    TileGame.dy = -2;
    TileGame.paddleHeight = 10;
    TileGame.paddleWidth = 75;
    TileGame.canvasHeight = 320;
    TileGame.canvasWidth = 480;
    TileGame.paddleX = (480 - TileGame.paddleWidth) / 2;
    TileGame.brickRowCount = 5;
    TileGame.brickColumnCount = 3;
    TileGame.brickWidth = 75;
    TileGame.brickHeight = 20;
    TileGame.brickPadding = 10;
    TileGame.brickOffsetTop = 30;
    TileGame.brickOffsetLeft = 30;
    TileGame.score = 0;
    TileGame.bricks = [];
    return TileGame;
}());
var Ball = /** @class */ (function () {
    function Ball() {
    }
    /**
     * Draw Ball on the Canvas
     */
    Ball.prototype.drawBall = function () {
        var can = document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        ctx.beginPath();
        ctx.arc(TileGame.x, TileGame.y, TileGame.ballRadius, 0, Math.PI * 2);
        ctx.fillStyle = "gray";
        ctx.fill();
        ctx.closePath();
    };
    return Ball;
}());
var Brick = /** @class */ (function () {
    function Brick() {
    }
    /**
     * Draw Bricks on Canvas
     */
    Brick.prototype.drawBrick = function () {
        var can = document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        for (var c = 0; c < TileGame.brickColumnCount; c++) {
            for (var r = 0; r < TileGame.brickRowCount; r++) {
                if (TileGame.bricks[c][r].status == 1) {
                    var brickX = (r * (TileGame.brickWidth + TileGame.brickPadding)) + TileGame.brickOffsetLeft;
                    var brickY = (c * (TileGame.brickHeight + TileGame.brickPadding)) + TileGame.brickOffsetTop;
                    TileGame.bricks[c][r].x = brickX;
                    TileGame.bricks[c][r].y = brickY;
                    ctx.beginPath();
                    ctx.rect(brickX, brickY, TileGame.brickWidth, TileGame.brickHeight);
                    ctx.fillStyle = "gray";
                    ctx.fill();
                    ctx.closePath();
                }
            }
        }
    };
    return Brick;
}());
var Paddle = /** @class */ (function () {
    function Paddle() {
    }
    /**
     * Draw Paddel on the Canvas
     */
    Paddle.prototype.drawPaddel = function () {
        var can = document.getElementById("myCanvas");
        var ctx = can.getContext("2d");
        ctx.rect(TileGame.paddleX, TileGame.canvasHeight - TileGame.paddleHeight, TileGame.paddleWidth, TileGame.paddleHeight);
        ctx.fillStyle = "gray";
        ctx.fill();
        ctx.closePath();
    };
    return Paddle;
}());
/**
 * Class for Handeling Key Events
 */
var EventHandler = /** @class */ (function () {
    function EventHandler() {
    }
    EventHandler.prototype.addEvent = function () {
        document.addEventListener("keydown", keyDownHandler, false);
        document.addEventListener("keyup", keyUpHandler, false);
        function keyDownHandler(e) {
            if (e.keyCode == 39) {
                TileGame.rightPressed = true;
            }
            else if (e.keyCode == 37) {
                TileGame.leftPressed = true;
            }
            else if (e.keyCode == 32) {
                var tileGame = new TileGame();
                //tileGame.callback();
                setInterval(tileGame.draw, 10);
            }
        }
        function keyUpHandler(e) {
            if (e.keyCode == 39) {
                TileGame.rightPressed = false;
            }
            else if (e.keyCode == 37) {
                TileGame.leftPressed = false;
            }
        }
    };
    return EventHandler;
}());
