/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var user;
var flag=0;

function getParameterByName(name, url)
{
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getInformation()
{
    var foo = getParameterByName('uname');
    user = JSON.parse(sessionStorage.getItem(foo));
    users=JSON.parse(localStorage.getItem("users"));
    for(temporaryVariable in users)
    {
        if(users[temporaryVariable].userName==user.userName)
        {
            document.getElementById('d').src = users[temporaryVariable].signUrl;
        }
    }    
}

/**
 * 
 * Used for tracking of pointer in the Canvas
 * 
 */
function init()
{
    var el = document.getElementById('c');
    var ctx = el.getContext('2d');
    var isDrawing;

    el.onmousedown = function (e) {
        isDrawing = true;
        ctx.moveTo(e.clientX, e.clientY);
    };
    el.onmousemove = function (e) {
        if (isDrawing) {
            ctx.lineTo(e.clientX, e.clientY);
            ctx.stroke();
            flag=1;
        }
    };
    el.onmouseup = function () {
        isDrawing = false;
    };
}

/**
 * 
 * Used for Clearing the Canvas
 * 
 */
function cleanCanvas()
{
    location.reload();
}

/**
 * 
 * Used for adding Signature in the local Storeage
 */

function addSignature()
{
    if(flag!=1)
    {
        alert("Nothing to Save");
        return;
    }
    var canvas = document.getElementById('c');
    var context = canvas.getContext('2d');
    var sign = canvas.toDataURL();
    u = JSON.parse(localStorage.getItem("users"));
    for(x in u)
    {
        if(u[x].userName==user.userName)
        {
            u[x].signUrl = sign;
            localStorage.setItem("users",JSON.stringify(u));
            break;
        }
    }
    document.getElementById('d').src = sign;
    alert("Signature Added Sucessfully");

}