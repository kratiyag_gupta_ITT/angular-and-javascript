/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
"use strict";
var flag = 0;
var flag2=0;
var notes = new Array();
/**
 * Opening and closing navigation window
 */
function openNav()
{
    if (flag === 0)
    {
        document.getElementById("sidebar").style.width = "30%";
        flag = 1;
    }
    else
    {
        document.getElementById("sidebar").style.width = "0";
        flag = 0;
    }
}

/**
 * Show editable Note window
 */
function showTextBox(id)
{
    document.getElementById("textbox-front").style.display = "none";
    document.getElementById("note").style.display = "block";
    document.getElementById("note-content").focus();
    if(id==2)
        {
            removeContent();
        }
}
/**
 * Save the Notes information to local storage
 **/
function saveData()
{
    var data = document.getElementById("note-content").innerText;
    var date = new Date();
    var title = document.getElementById("title").value;
    var imageData = document.getElementById("uploaded-image").src;
    var id = getId();
    /**
     * Validation Conditiion for data
     */
    if (((data === "")||(!data.trim(" ").length>0)) && imageData==location.href)
    {
        alert("Require either image or data in note to save Data");
        document.getElementById("note-content").focus();
        return;
    }
    try
    {
        saveInLocalStorage(notes, new notesData(id, date.getDate()+"/"+(date.getMonth()+1)+"  "+date.getHours()+":"+date.getMinutes(), data, title, imageData));
    }
    catch(e)
    {
        consol.log(e);
    }
    
    console.log("data saved");
    console.log(date);
    document.getElementById("note-content").innerHTML = "";
    document.getElementById("title").value = "";
    document.getElementById("uploaded-image").src = "";
    document.getElementById("first-half").innerHTML = "";
    document.getElementById("second-half").innerHTML = "";
    renderOldData(0);
    document.getElementById("textbox-front").style.display = "block";
    document.getElementById("note").style.display = "none";
    document.getElementById("note-content").innerHTML="<div class='text-area-placeholder'><input type='text' id='text-holder' style='border: none;' placeholder='Start writing note.....' /></div>";
    document.getElementById("textbox-front").focus();
    flag2=0;
}
/**
 * Constructor for object Initialization
 * @param {type} id         Note Id
 * @param {type} date       Note Creation Time
 * @param {type} data       Note Data
 * @param {type} title      Note Title
 * @param {type} imageData  Note Image
 * @returns {notesData}     
 */
function notesData(id, date, data, title, imageData)
{
    this.id = id;
    this.date = date;
    this.data = data;
    this.title = title;
    this.imageData = imageData;
}
/**
 * Saves the Notes Data in Local Storage
 */
function saveInLocalStorage(array, noteData)
{
    var array;
    if (localStorage.getItem("userData") === null)
    {
        array.push(noteData);
        localStorage.setItem("userData", JSON.stringify(array));
    }
    else
    {
        array = JSON.parse(localStorage.getItem("userData"));
        array.push(noteData);
        localStorage.setItem("userData", JSON.stringify(array));
    }
}

/**
 * read URL of image
 * @param {type} hold data
 * @returns {undefined}
 */
function readURL(input)
{
    document.getElementById("uploaded-image").style.display = "block";

    if (input.files && input.files[0])
    {
        var reader = new FileReader();
        reader.onload = function (e)
        {
            document.getElementById('uploaded-image').src = e.target.result;

        };

        reader.readAsDataURL(input.files[0]);
    }
}
/**
 * Display old data on lower display
 * @param {type} less no of tiles not to be display on screen
 * @returns {undefined}
 */
function renderOldData(less)
{
    var i;
    var array;
    var len;
    var flag = 0;
    var max = getId();
    var container1 = document.getElementById("first-half");
    var container2 = document.getElementById("second-half");
    var tile;

    document.getElementById("textbox-front").focus();
    if (localStorage.getItem("userData") !== null)
    {
        array = JSON.parse(localStorage.getItem("userData"));
        len = array.length;
    }
    
    for (i = len - 1; i >= 0; i--)
    {
        var tilesFooter = ['<div class="tiles-footer"><label for="footer-image-icon' + array[i].id + '" ><span class="glyphicon glyphicon-picture cursor-pointer" title="add image">\n\
                            </label><input type="file" style="display:none" id="footer-image-icon' + array[i].id + '" onchange="uploadImage(this,this.id)"/>\n\
                            </span><span class="glyphicon glyphicon-trash  cursor-pointer" data-toggle="modal" data-target="#myModel" id="' +array[i].id + '" onclick="deleteNote(this.id)" title="delete Note"></span>\n\
                            <span class="glyphicon glyphicon-pencil  cursor-pointer" id="' +array[i].id + '" data-toggle="modal" data-target="#myModal" onclick="editData(this.id)"></span><span>'+array[i].date+'</span>\n\
                            </div>'];
        tile = "<div id='draggable' style='decoration:none' class='well insingle-line'><img id='image" + array[i].id + "' style='max-height:300px;width:100%;' src='" + array[i].imageData + "' class='img-responsive'/><div class='title-font'>" + array[i].title +
                "</div><div class='data-font' id='" + array[i].id + "' data-toggle='modal' data-target='#myModal' onclick='editData(this.id)'>" + array[i].data + "</div>" + tilesFooter + "</div>";
        if (flag === 0)
        {
            container1.innerHTML = container1.innerHTML + tile;
            flag = 1;
        }
        else
        {
            container2.innerHTML += tile;
            flag = 0;
        }
    }

}

/**
 * get Id of document
 * @returns {Number|JSON@call;parse.length|array.length}
 */
function getId()
{
    var array,trash,i,max1,max2;
    max1=0;
    max2=0;
    if(localStorage.getItem("trash")==null)
    {
        max2=0;
    }
    else
    {
        trash=JSON.parse(localStorage.getItem("trash"));
        for(i=0;i<trash.length;i++)
        {
            if(max2<trash[i].id)
            {
                max2=trash[i].id;
            }
        }
    }
    if(localStorage.getItem("userData")==null)
    {
        max1=0;
    }
    else
    {
        array=JSON.parse(localStorage.getItem("userData"));
        for(i=0;i<array.length;i++)
        {
            if(max1<array[i].id)
            {
                max1=array[i].id;
            }
        }
    }
    if(max1>max2)
        return max1+1;
    else
        return max2+1;
}
/**
 * Delete the Data
 * @param {type} id
 * @returns {undefined}
 */
function deleteNote(id)
{
    var array = JSON.parse(localStorage.getItem("userData"));
    var i;
    for (i = 0; i < array.length+1; i++)
    {

        if (array[i].id == id)
        {
            trashGenerator(array[i]);
            array.splice(i, 1);
            break;
        }
    }
    localStorage.setItem("userData", JSON.stringify(array));
    document.getElementById("first-half").innerHTML = "";
    document.getElementById("second-half").innerHTML = "";
    renderOldData(0);
            
}
/**
 * Upload image after Storing Data
 * @param {type} input
 * @param {type} id
 */
function uploadImage(input, id)
{
    var array = JSON.parse(localStorage.getItem("userData"));
    var i;
    if (input.files && input.files[0])
    {
        var reader = new FileReader();

        reader.onload = function (e)
        {
            for (i = 0; i < array.length; i++)
            {
                if (id.slice(17, 19) == array[i].id + "")
                {
                    document.getElementById("image" + id.slice(17, 19)).src = e.target.result;
                    array[i].imageData = e.target.result;
                }
            }

            localStorage.setItem("userData", JSON.stringify(array));
        }
        reader.readAsDataURL(input.files[0]);
    }
}
/**
 * Moves the Data to Trash Array
 * @param {type} trashData holdes the object of trash
 * @returns {undefined}
 */
function trashGenerator(trashData)
{
    var array;
    if (localStorage.getItem("trash") == null)
    {
        array = new Array();
        array.push(trashData);
        localStorage.setItem("trash", JSON.stringify(array));
    }
    else
    {
        array = new Array();
        array = JSON.parse(localStorage.getItem("trash"));
        array.push(trashData);
        localStorage.setItem("trash", JSON.stringify(array));
    }
}
/**
 * Add data to pop-up from memory
 * @param {type} holdes the Id of data stored
 * @returns {undefined}
 */
function editData(id)
{
    var i, length;
    var array;
    array = JSON.parse(localStorage.getItem("userData"));
    length = array.length;

    for (i = 0; i < length; i++)
    {
        if (array[i].id == id)
        {
            document.getElementById("model-title").value = array[i].title;
            document.getElementById("model-note-content").innerHTML = array[i].data;
            document.getElementById("id-holder").value = array[i].id;
        }
    }
}
/**
 * Edit the Node Data Using Pop ups
 * @param {type} id  Holdes Id of edited tiles
 * @returns {undefined}
 */
function storeModelData(id)
{
    var i, length, title, content;
    var array;
    array = JSON.parse(localStorage.getItem("userData"));
    length = array.length;
    title = document.getElementById("model-title").value;
    content = document.getElementById("model-note-content").innerText;
    if(content=="")
        {
            alert("Data Cannot be Empty");
            return;
        }
    for (i = 0; i < length; i++)
    {
        if (array[i].id == id)
        {
            array[i].title = title;
            array[i].data = content;
        }
    }
    localStorage.setItem("userData", JSON.stringify(array));
    document.getElementById("first-half").innerHTML = "";
    document.getElementById("second-half").innerHTML = "";
    renderOldData(0);
}
/**
 * Enabling and Disabling Done Button on Model
 */
function enableModelButton()
{
    var content;
    content = document.getElementById("model-note-content").innerText;
    if (content == "")
    {
        document.getElementById("id-holder").disabled = true;
    }
    else
    {
        document.getElementById("id-holder").disabled = false;
    }
}
/**
 * Changing the View to Tiles and Grid   
 * @param {type} id     Holdes the Id of image icon 
 */
function changeView(id)
{
    if (id == "list-icon")
    {
        document.getElementById("grid-icon").style.display = "inline";
        document.getElementById("list-icon").style.display = "none";
        document.getElementById("first-half-wrapper").className = "col-md-12";
        document.getElementById("second-half-wrapper").className = "col-md-12";
    }
    if (id == "grid-icon")
    {
        document.getElementById("grid-icon").style.display = "none";
        document.getElementById("list-icon").style.display = "inline";
        document.getElementById("first-half-wrapper").className = "col-md-6";
        document.getElementById("second-half-wrapper").className = "col-md-6";
    }

}
/**
 * Cancel Entring the Date
 */
function cancel()
{
    document.getElementById("uploaded-image").src="";
    document.getElementById("title").value="";
    document.getElementById("note-content").innerHTML="<div class='text-area-placeholder'><input type='text' id='text-holder' style='border: none;' placeholder='Start writing note.....' /></div>";
    document.getElementById("textbox-front").style.display = "block";
    document.getElementById("note").style.display = "none";
    document.getElementById("textbox-front").focus();
    flag2=0;
}
/**
 * Remove Content
 */
function removeContent()
{
    if(flag2==0)
        {
            document.getElementById("note-content").innerHTML="";
        }
    flag2=1;    
}
/**
 * Sortible UI using Jquery
 */
$(function () {
    $("#first-half").sortable({
        revert: true
    });

    $("ul, li").disableSelection();
});

/**
 * Sortable UI using JQuery
 */
$(function () {
    $("#second-half").sortable({
        revert: true
    });

    $("ul, li").disableSelection();
});
