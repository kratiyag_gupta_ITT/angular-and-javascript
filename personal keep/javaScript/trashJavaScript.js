/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

"use strict";
var flag = 0;
var notes = new Array();
/**
 * Restores Data back to user main Storage
 */
function restoreData(id)
{

    var userData;
    var trash, i, arrayLength;
    if (localStorage.getItem("userData") == null)
    {
        localStorage.setItem("userData", new Array());
    }
    userData = JSON.parse(localStorage.getItem("userData"));
    trash = JSON.parse(localStorage.getItem("trash"));
    arrayLength = trash.length;
    for (i = 0; i < arrayLength; i++)
    {
        if (trash[i].id == id+"")
        {
            userData.push(trash[i]);
            trash.splice(i, 1);
            break;
        }
    }
    localStorage.setItem("userData", JSON.stringify(userData));
    localStorage.setItem("trash", JSON.stringify(trash));
    document.getElementById("first-half").innerHTML = "";
    document.getElementById("second-half").innerHTML = "";
    renderOldData(0);
}

/**
 * Open side Navigation Window
 */

function openNav()
{
    if (flag === 0)
    {
        document.getElementById("sidebar").style.width = "30%";
        flag = 1;
    }
    else
    {
        document.getElementById("sidebar").style.width = "0";
        flag = 0;
    }
}

/**
 * Display old data on lower display
 * @param {type} less no of tiles not to be display on screen
 * @returns {undefined}
 */
function renderOldData(less)
{

    var i;
    var array;
    var len;
    var flag = 0;
    var container1 = document.getElementById("first-half");
    var container2 = document.getElementById("second-half");
    var tile;

    if (localStorage.getItem("trash") !== null)
    {
        array = JSON.parse(localStorage.getItem("trash"));
    }
    len = array.length;
    for (i = len - 1 - less; i >= 0; i--)
    {
        var tilesFooter = ['<div class="tiles-footer"><span class="glyphicon glyphicon-trash  cursor-pointer" id="' + array[i].id + '" onclick="deleteNote(this.id)" title="delete Note"></span><span class="glyphicon glyphicon-upload cursor-pointer " id="' + array[i].id + '" title="restore data" onclick="restoreData(this.id)"></span></div>'];
        tile = "<div id='draggable' style='decoration:none' class='well insingle-line'><img id='image" + array[i].id + "'  style='max-height:300px;' src='" + array[i].imageData + "' class='img-responsive'/><div class='title-font'>" + array[i].title +
                "</div><div class='data-font' id='" + array[i].id + "'onclick='editData(this.id)'>" + array[i].data + "</div>" + tilesFooter + "</div>";
        if (flag === 0)
        {
            container1.innerHTML = container1.innerHTML + tile;
            flag = 1;
        }
        else
        {
            container2.innerHTML += tile;
            flag = 0;
        }
    }

}

/**
 * Delete the Data
 * @param {type} id Holdes Id of Note
 */
function deleteNote(id)
{
    var i;
    var array = JSON.parse(localStorage.getItem("trash"));
    for (i = 0; i < array.length; i++)
    {
        if (array[i].id == id)
        {
            array.splice(i, 1);
            break;
        }
    }
    localStorage.setItem("trash", JSON.stringify(array));
    document.getElementById("first-half").innerHTML = "";
    document.getElementById("second-half").innerHTML = "";
    renderOldData(0);
}
